import csv

def imc_calculator(pesoPerson, alturaPerson):
    return pesoPerson / (alturaPerson * alturaPerson)

def name_formatter(firstName, lastName):
    nameFormated = f'{firstName} {lastName}'
    return nameFormated.upper()

fileReaderCsv = 'DATASET.CSV'
fileWriterTxt = 'ThiagoCalmonAmorim.txt'

with open(fileReaderCsv, 'r') as database, open(fileWriterTxt, 'w') as resultsDataFormated:
    readerCSV = csv.reader(database, delimiter=';')
    
    next(readerCSV)
    
    for row in readerCSV:
        
        #Notei dados incompletos, então criei também esse tratamento para esses dados
        if len(row) < 4 or any(not campo.strip() for campo in row):
            resultsDataFormated.write('DADOS INCOMPLETOS' + '\n')
            continue
        
        
        primeiroNome, ultimoNome, peso, altura = row
        
        primeiroNome = primeiroNome.strip()
        ultimoNome = ultimoNome.strip()
        
        if ',' in peso:
            peso = float(peso.replace(',', '.'))
        else: 
            peso = float(peso)  
            
        if ',' in altura:
            altura = float(altura.replace(',', '.'))
        else: 
            altura = float(altura)
        
        nomeCompleto =  name_formatter(primeiroNome, ultimoNome)
        imc = imc_calculator(peso, altura)
        result = f'´{nomeCompleto} {imc: .2f}'.replace('.', ',')
        #Identifiquei o formato do imc soliticado com a "," como ponto flutuante como regra de negócio por isso o replace
        
        resultsDataFormated.write(result + '\n')
        
        